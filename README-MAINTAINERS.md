# Cómo añadir TestX para el ejercicio e?

1. añadir al fichero ```tests``` la línea ```ayedd.e?.TestX```

2. copiar plantilla de fichero LEEME en el directorio del ejercicio:

    ```src/main/java/ayedd/e?/LEEME```

3. copiar fichero TestX.java.orig en el directorio de los tests del ejercicio:

    ```src/test/java/ayedd/e?/TestX.java.orig```
       
